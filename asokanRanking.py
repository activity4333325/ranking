import sys
from itertools import combinations as nCr
COMMENT = '#'


def load_data(marks_file: str) -> list[tuple]:
    with open(marks_file) as f:
        lines = [line.strip() for line in f if line[0] != COMMENT]
    data = []
    for line in lines:
        name, *raw_marks = line.split()
        marks = [int(_) for _ in raw_marks]
        data.append((name, marks))
    return data


def relation(a_student: tuple, b_student: tuple) -> str:
    a_name, a_marks = a_student
    b_name, b_marks = b_student
    if all(a < b for a, b in zip(a_marks, b_marks)):
        return f"{a_name}<{b_name}"
    elif all(a > b for a, b in zip(a_marks, b_marks)):
        return f"{b_name}<{a_name}"
    else:
        return f"{a_name}#{b_name}"


def all_relations(data: list[tuple]) -> list[str]:
    return [relation(alpha, beta) for alpha, beta in nCr(data, 2)]


def comparables(data: list[tuple]) -> list[str]:
    return [(rel[0], rel[-1]) for rel in all_relations(data) if '#' not in rel]


def remove_redundancies(data: list[tuple]) -> set[str]:
    relations = comparables(data)
    firsts = set([a[0] for a in relations])
    seconds = set([a[1] for a in relations])
    both = firsts & seconds
    redundancies = set()
    for a in firsts:
        for c in seconds:
            for b in both:
                if (a, b) in relations and\
                   (b, c) in relations and\
                   (a, c) in relations:
                    redundancies.add((a, c))
    return set(relations) - redundancies


data = load_data(sys.argv[1])
print(data)
# print(relation(data[0], data[1]))
# print(relation(data[3], data[2]))
# print(relation(data[2], data[5]))
print(all_relations(data))
print(comparables(data))
print(remove_redundancies(data))
