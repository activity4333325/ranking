def read_students(filename):
    students = {}
    with open(filename, 'r') as f:
        for line in f:
            data = line.split()
            name = data[0]
            marks = list(map(int, data[1:]))
            students[name] = marks
    return students

def compare_students(students):
    comparisons = []
    for name1, marks1 in students.items():
        for name2, marks2 in students.items():
            if name1 != name2:
                if all(m1 > m2 for m1, m2 in zip(marks1, marks2)):
                    comparisons.append((name2 + "<" + name1))
                # elif all(m1 < m2 for m1, m2 in zip(marks1, marks2)):
                #     comparisons.append((name1, name2))
    return comparisons

# Example usage:
filename = "/Users/a91986/Downloads/studentRanking.txt"
result_graph = compare_students(read_students(filename))
print(result_graph)

